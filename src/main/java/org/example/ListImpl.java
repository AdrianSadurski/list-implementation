package org.example;

public class ListImpl {

    public void add(int element) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        throw new UnsupportedOperationException();
    }

    public boolean isEmpty() {
        throw new UnsupportedOperationException();
    }

    public boolean contains(int element) {
        throw new UnsupportedOperationException();
    }
}
