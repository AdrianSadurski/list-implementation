package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ListImplTest {

    private ListImpl list;

    @BeforeEach
    void init() {
        list = new ListImpl();
    }

    @Test
    void shouldReturnSize() {
        assertEquals(0, list.size());
    }

    @Test
    void shouldReturnTrueWhenListIsEmpty() {
        assertTrue(list.isEmpty());
    }

    @Test
    void shouldAddElement() {
        list.add(1);

        assertFalse(list.isEmpty());
    }

    @Test
    void shouldAddMultipleElement() {
        list.add(1);
        list.add(2);
        list.add(3);

        assertEquals(3, list.size());
    }

    @Test
    void shouldReturnTrueWhenContainsElement() {
        list.add(1);
        list.add(2);
        list.add(3);

        assertTrue(list.contains(2));
    }
}